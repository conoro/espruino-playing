var RX_DR = 6;
var TX_DS = 5;
var MAX_RT = 4;
var RX_P_NO = 1;
var TX_FULL = 0;

var CONFIG = 0x00;
var EN_AA = 0x01;
var EN_RXADDR = 0x02;
var SETUP_AW = 0x03;
var SETUP_RETR = 0x04;
var RF_CH = 0x05;
var RF_SETUP = 0x06;
var RX_ADDR_P0 = 0x0A;
var RX_ADDR_P1 = 0x0B;
var RX_ADDR_P2 = 0x0C;
var RX_ADDR_P3 = 0x0D;
var RX_ADDR_P4 = 0x0E;
var RX_ADDR_P5 = 0x0F;
var TX_ADDR = 0x10;
var RX_PW_P0 = 0x11;
var RX_PW_P1 = 0x12;
var RX_PW_P2 = 0x13;
var RX_PW_P3 = 0x14;
var RX_PW_P4 = 0x15;
var RX_PW_P5 = 0x16;
var FIFO_STATUS = 0x17;
var DYNPD = 0x1C;
var FEATURE = 0x1D;

var RF_DR_LOW = 5;
var RF_DR_HIGH = 3;
var RF_PWR_LOW = 1;
var RF_PWR_HIGH = 2;

var EN_CRC = 3;
var CRCO = 2;


SPI1.setup({sck:A5, miso:A6, mosi:A7});
var nrf = require("NRF24L01P").connect( SPI1, B0, B1 );
function onInit() {
  nrf.init([0,0,0,0,2], [0xFE,0xFE,0xFE,0xFE,0xFE]);
}
onInit();
status = nrf.getStatus();

function BV(x){
 return (1<<(x));
}

// Status Register
ptemp = "STATUS\t\t = 0x" + status.toString(16);
console.log(ptemp);
ptemp = "RX_DR = 0x" + ((status & BV(RX_DR)?1:0).toString(16));
console.log (ptemp);
ptemp = "TX_DS = 0x" + ((status & BV(TX_DS)?1:0).toString(16));
console.log (ptemp);
ptemp = "MAX_RT = 0x" + ((status & BV(MAX_RT)?1:0).toString(16));
console.log (ptemp);
ptemp = "RX_P_NO = 0x" + (((status >> RX_P_NO) & 7).toString(16));
console.log (ptemp);
ptemp = "TX_FULL = 0x" + ((status & BV(TX_FULL)?1:0).toString(16));
console.log (ptemp);

// Status = 0x42 = 1000010
// Reserved: Bit 7 = 0, Reserved, Always 0
// RX_DR:    Bit 6 = 1, Reset value = 0, Data Ready RX FIFO interrupt
// TX_DS:    Bit 5 = 0, Reset value = 0, Data Sent TX FIFO interrupt
// MAX_RT:   Bit 4 = 0, Reset value = 0, Max num TX re-transmits interrupt
// RX_P_NO:  Bit 3:1 = 001, Reset value = 111, Data pipe number for payload. So 001 = Second Pipe
// TX_FULL:  Bit 0 = 0, Reset value = 0, TX FIFO full


// RX and TX Address Registers
console.log("RX_ADDR_P0 = " + nrf.getAddr(RX_ADDR_P0));
console.log("RX_ADDR_P1 = " + nrf.getAddr(RX_ADDR_P1));
console.log("RX_ADDR_P2 = " + nrf.getAddr(RX_ADDR_P2));
console.log("RX_ADDR_P3 = " + nrf.getAddr(RX_ADDR_P3));
console.log("RX_ADDR_P4 = " + nrf.getAddr(RX_ADDR_P4));
console.log("RX_ADDR_P5 = " + nrf.getAddr(RX_ADDR_P5));
console.log("TX_ADDR = " + nrf.getAddr(TX_ADDR));

// More settings
console.log("RX_PW_P0 = 0x" + nrf.getReg(RX_PW_P0).toString(16));
console.log("RX_PW_P1 = 0x" + nrf.getReg(RX_PW_P1).toString(16));
console.log("RX_PW_P2 = 0x" + nrf.getReg(RX_PW_P2).toString(16));
console.log("RX_PW_P3 = 0x" + nrf.getReg(RX_PW_P3).toString(16));
console.log("RX_PW_P4 = 0x" + nrf.getReg(RX_PW_P4).toString(16));
console.log("RX_PW_P5 = 0x" + nrf.getReg(RX_PW_P5).toString(16));
console.log("RX_PW_P6 = 0x" + nrf.getReg(RX_PW_P6).toString(16));
console.log("EN_AA = 0x" + nrf.getReg(EN_AA).toString(16));
console.log("EN_RXADDR = 0x" + nrf.getReg(EN_RXADDR).toString(16));
console.log("RF_CH = 0x" + nrf.getReg(RF_CH).toString(16));
console.log("RF_SETUP = 0x" + nrf.getReg(RF_SETUP).toString(16));
console.log("CONFIG = 0x" + nrf.getReg(CONFIG).toString(16));
console.log("DYNPD = 0x" + nrf.getReg(DYNPD).toString(16));
console.log("FEATURE = 0x" + nrf.getReg(FEATURE).toString(16));

// Data Rate
var dr = nrf.getReg(RF_SETUP) & (BV(RF_DR_LOW) | BV(RF_DR_HIGH));
if ( dr == BV(RF_DR_LOW) ){
  console.log("Data Rate = 250KBPS");
} else if ( dr == BV(RF_DR_HIGH) ){
  console.log("Data Rate = 2MBPS");
}else{
  console.log("Data Rate = 1MBPS");
}

// CRC Length
var config = nrf.getReg(CONFIG) & ( BV(CRCO) | BV(EN_CRC)) ;

if ( config & BV(EN_CRC ) ){
  if ( config & BV(CRCO) )
    console.log("CRC Length = 16");
  else
    console.log("CRC Length = 8");
}

// PA Power
var power = nrf.getReg(RF_SETUP) & (BV(RF_PWR_LOW) | BV(RF_PWR_HIGH));
if ( power == (BV(RF_PWR_LOW) | BV(RF_PWR_HIGH)) ){
  console.log("PA Power = MAX");
}else if ( power == BV(RF_PWR_HIGH) ){
  console.log("PA Power = HIGH");
}else if ( power == BV(RF_PWR_LOW) ){
  console.log("PA Power = LOW");
}else{
  console.log("PA Power = MIN");
}
