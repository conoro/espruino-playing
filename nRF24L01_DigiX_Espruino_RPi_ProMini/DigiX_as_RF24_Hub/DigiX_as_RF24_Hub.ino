/*
 This program is the receiver for nRF24_Send_to_RPi that was originally written 
 for the Raspberry Pi and ported back to UNO for completeness.

 Written by Stanley Seow
 stanleyseow@gmail.com
*/

#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

RF24 radio(53,52);

// Radio pipe addresses for the 2 nodes to communicate.
//const uint64_t pipes[6] = { 0xF0F0F0F0D2LL, 0xF0F0F0F0E1LL, 0xF0F0F0F0E2LL, 0xF0F0F0F0E3LL, 0xF0F0F0F0E4LL, 0xF0F0F0F0E5LL };
// bytes serv1 = 0x7365727631 in hex 
const uint64_t pipes[6] = { 0x0B0B0B0B0BLL, 0x0B0B0B0B0BLL, 0x0B0B0B0B0CLL, 0x0000000003LL, 0x0000000004LL, 0x0000000005LL };


void setup(void)
{
  digitalWrite(2,HIGH);
  delay(500);
  digitalWrite(2,LOW);
  
  Serial.begin(57600);

  radio.begin();
  
  radio.setDataRate(RF24_2MBPS);
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(0);
  radio.enableDynamicPayloads();
  radio.setRetries(15,15);
  
  radio.setCRCLength(RF24_CRC_8);

  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);
  radio.openReadingPipe(2,pipes[2]);
  radio.openReadingPipe(3,pipes[3]);
  radio.openReadingPipe(4,pipes[4]);
  radio.openReadingPipe(5,pipes[5]);

  radio.startListening();
  radio.printDetails();
  
  delay(1000);
}

void loop(void)
{ 
    char receivePayload[31];
    uint8_t len = 0;
    uint8_t pipe = 0;
        
        
    // Loop thru the pipes 0 to 5 and check for payloads    
    if ( radio.available( &pipe ) ) {
      bool done = false;
      while (!done)
      {
        len = radio.getDynamicPayloadSize();  
        done = radio.read( &receivePayload,len );
        
        // Sending back reply to sender using the same pipe
        radio.stopListening();
        //radio.openWritingPipe(pipes[pipe]);
        radio.write(receivePayload,len);
        
        // Format string for printing ending with 0
        receivePayload[len] = 0;
        Serial.print("Got payload: ");
        Serial.print(receivePayload);
        Serial.print(" len: ");
        Serial.print(len);
        Serial.print(" pipe: ");
        Serial.print(pipe);
        Serial.println();
        
        radio.startListening();
        
        // Increase pipe and reset to 0 if more than 5
        pipe++;
        if ( pipe > 5 ) pipe = 0;
      }

    }

delay(20);

}
