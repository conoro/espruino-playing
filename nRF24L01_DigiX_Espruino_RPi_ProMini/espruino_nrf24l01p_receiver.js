SPI1.setup({sck:A5, miso:A6, mosi:A7});
var nrf = require("NRF24L01P").connect( SPI1, B0, B1 );
function onInit() {
  nrf.init([0,0,0,0,2], [0xFE,0xFE,0xFE,0xFE,0xFE]);
}
onInit();

dataLine = "";
setInterval(function() {
  while (nrf.dataReady()) {
    var data = nrf.getData();
    for (var i in data) {
      var ch = data[i];
      if (ch===0 && dataLine!=="") {
        console.log(dataLine);
        // we could even save it onto an SD card using require('fs').appendFile("log.txt", dataLine+"\n");
        dataLine = "";
      } else if (ch!==0) {
        dataLine += String.fromCharCode(ch);
      }
    }
  }
}, 50);
